# Demo Elastic

## Overview
This project is a Spring Boot application that integrates Elasticsearch for search functionality. It utilizes various search types to retrieve documents from an Elasticsearch index.

## Dependencies
- Elasticsearch: For indexing and searching documents.
- Lombok: For reducing boilerplate code in Java classes.
- Jackson: For JSON serialization and deserialization.

## Search Types
### Match
- **Description**: Find some objects that match a specific field.
- **Usage Example**: Search for documents where a particular field matches a specified value.

### Wildcard
- **Description**: Find some objects using a wildcard expression.
- **Usage Example**: Search for documents using wildcard patterns in the query.

### Match All
- **Description**: Find all the documents from a specific index.
- **Usage Example**: Retrieve all documents from the Elasticsearch index.

### Fuzzy
- **Description**: Find some objects that sound alike.
- **Usage Example**: Search for documents that are similar in pronunciation to a given term.

### Bool
- **Description**: Find some objects that match with multiple queries together.
- **Usage Example**: Construct complex queries using boolean logic (must, should, must_not, filter, etc.) to filter documents.

### Multi Match
- **Description**: Find some objects that match with multiple fields.
- **Usage Example**: Search for documents that match a keyword across multiple specified fields.

## Getting Started
1. Clone this repository.
2. Configure Elasticsearch settings in `application.yml`.
3. Run the Spring Boot application.

## Usage
- Implement controllers and services to handle search requests.
- Use appropriate Elasticsearch query types based on the search requirements.
- Test search functionality using tools like Postman or through unit tests.