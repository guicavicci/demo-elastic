package com.example.demoelastic.controller;

import com.example.demoelastic.controller.dto.GetProductBoolDTO;
import com.example.demoelastic.entity.Product;
import com.example.demoelastic.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/products")
@Slf4j
public class ProductController {

    private final ProductService productService;

    @PostMapping()
    public ResponseEntity<Product> insertProduct(@RequestBody Product product) {
        var productCreated = productService.insertProduct(product);
        return ResponseEntity.ok(productCreated);
    }

    @GetMapping()
    public Iterable<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/match-all")
    public List<Product> matchAllProducts() {
        return productService.matchAllProducts();
    }

    @GetMapping("/match-name/{name}")
    public List<Product> matchProductsWithName(@PathVariable String name) {
        return productService.matchProductsWithName(name);
    }

    @GetMapping("/match-fuzzy/{name}")
    public List<Product> matchProductsWithFuzzyName(@PathVariable String name) {
        return productService.matchFuzzyName(name);
    }

    @PostMapping("/match-bool/search")
    public List<Product> matchBoolProducts(@RequestBody GetProductBoolDTO getProductBoolDTO) {
        return productService.matchBool(getProductBoolDTO);
    }

    @GetMapping("/match-multi-query")
    public List<Product> matchMultiQueryProducts(@RequestParam String key, @RequestParam List<String> fields) {
        return productService.matchMultiQuery(key, fields);
    }

    @GetMapping("/match-bool-filter")
    public List<Product> matchMultiQueryProducts(@RequestParam Map<String, String> fields) {
        return productService.matchBoolFilter(fields);
    }

    @PutMapping()
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        var productUpdated = productService.updateProduct(product);
        return ResponseEntity.ok(productUpdated);
    }
}
