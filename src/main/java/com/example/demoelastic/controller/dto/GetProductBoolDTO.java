package com.example.demoelastic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GetProductBoolDTO {
    private Map<String, String> mapFields;
    private Integer quantity;
}
