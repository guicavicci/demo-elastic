package com.example.demoelastic.util;

import co.elastic.clients.elasticsearch._types.query_dsl.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class ElasticSearchUtil {

    public static Supplier<Query> supplier() {
        return () -> Query.of(q -> q.matchAll(matchAllQuery()));

    }
    public static MatchAllQuery matchAllQuery() {
        return new MatchAllQuery.Builder().build();
    }

    public static Supplier<Query> supplierWithFields(String name) {
        return () -> Query.of(q -> q.match(matchQueryWithName(name)));

    }
    public static MatchQuery matchQueryWithName(String name) {
        var matchQuery = new MatchQuery.Builder()
                .field("name")
                .query(name)
                .build();

        return matchQuery;
    }

    public static Supplier<Query> supplierFuzzyName(String name) {
        return () -> Query.of(q -> q.fuzzy(createFuzzyQuery(name)));

    }

    public static FuzzyQuery createFuzzyQuery(String name) {
        return new FuzzyQuery.Builder()
                .field("name")
                .value(name)
                .fuzziness("AUTO")
                .build();
    }

    public static Supplier<Query> supplierBoolQuery(Map<String, String> mapFields, Integer quantity) {
        return () -> Query.of(q -> q.bool(boolQuery(mapFields, quantity)));
    }

    public static BoolQuery boolQuery(Map<String, String> mapFields, Integer quantity) {
        var boolQuery = new BoolQuery.Builder();
        return boolQuery.filter(createTermQuery(mapFields)).must(createMatchQuery(quantity)).build();
    }

    private static List<Query> createTermQuery(Map<String, String> mapFields) {
        final List<Query> terms = new ArrayList<>();
        mapFields.forEach((key, value) -> {
            var termQuery = new TermQuery.Builder();
            terms.add(Query.of(q -> q.term(termQuery.field(key).value(value).build())));
        });
        return terms;
    }

    private static List<Query> createMatchQuery(Integer quantity) {
        final List<Query> matches = new ArrayList<>();
        var matchQuery = new MatchQuery.Builder();
        matches.add(Query.of(q -> q.match(matchQuery.field("quantity").query(quantity).build())));
        return matches;
    }

    public static Supplier<Query> supplierMultiMatchQuery(String key, List<String> values) {
        return () -> Query.of(q -> q.multiMatch(multiMatchQueryWithName(key, values)));
    }

    public static MultiMatchQuery multiMatchQueryWithName(String key, List<String> values) {
        var multiMatchQuery = new MultiMatchQuery.Builder()
                .fields(values)
                .query(key)
                .build();
        return multiMatchQuery;
    }

    public static Supplier<Query> supplierBoolQueryFilter(Map<String, String> mapFields) {
        return () -> Query.of(q -> q.bool(boolQueryFilter(mapFields)));
    }

    public static BoolQuery boolQueryFilter(Map<String, String> mapFields) {
        var boolQuery = new BoolQuery.Builder();
        return boolQuery.filter(createTermQuery(mapFields)).build();
    }
}
