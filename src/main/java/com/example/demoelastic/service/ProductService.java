package com.example.demoelastic.service;

import co.elastic.clients.elasticsearch.core.search.Hit;
import com.example.demoelastic.controller.dto.GetProductBoolDTO;
import com.example.demoelastic.entity.Product;
import com.example.demoelastic.repo.ProductRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepo productRepo;

    private final ElasticSearchProductService elasticSearchProductService;

    public Product insertProduct(Product product) {
        return productRepo.save(product);
    }

    public Product updateProduct(Product product) {
        Product productFromDb = productRepo.findById(product.getId())
                .orElseThrow(() -> new RuntimeException("Product not found"));
        productFromDb.setPrice(product.getPrice());
        return productRepo.save(productFromDb);
    }

    public Iterable<Product> getAllProducts() {
        return productRepo.findAll();
    }

    public Product deleteProduct(Integer id) {
        Product product = productRepo.findById(id)
                .orElseThrow(() -> new RuntimeException("Product not found"));
        productRepo.delete(product);
        return product;
    }

    public List<Product> matchAllProducts() {
        var searchResponse = elasticSearchProductService.matchAllProductServices();
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }

    public List<Product> matchProductsWithName(String name) {
        var searchResponse = elasticSearchProductService.matchProductsWithName(name);
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }

    public List<Product> matchFuzzyName(String name) {
        var searchResponse = elasticSearchProductService.matchProductsWithFuzzyName(name);
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }

    public List<Product> matchBool(GetProductBoolDTO getProductBoolDTO) {
        var searchResponse = elasticSearchProductService.matchProductsWithBoolQuery(getProductBoolDTO.getMapFields(), getProductBoolDTO.getQuantity());
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }

    public List<Product> matchMultiQuery(String key, List<String> values) {
        var searchResponse = elasticSearchProductService.matchProductsWithMultiMatchQuery(key, values);
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }

    public List<Product> matchBoolFilter(Map<String, String> fields) {
        var searchResponse = elasticSearchProductService.matchProductsBoolQueryFilter(fields);
        log.info(searchResponse.hits().hits().toString());
        return searchResponse.hits().hits().stream().map(Hit::source).collect(Collectors.toList());
    }
}
