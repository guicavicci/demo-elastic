package com.example.demoelastic.service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.example.demoelastic.entity.Product;
import com.example.demoelastic.util.ElasticSearchUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Service
@Slf4j
public class ElasticSearchProductService {

    private final ElasticsearchClient elasticsearchClient;
    private static final String PRODUCT_INDEX = "products";

    public SearchResponse<Map> matchAllServices() {
        SearchResponse<Map> searchResponse = null;

        try {
            var supplier = ElasticSearchUtil.supplier();
            log.info(supplier.get().toString());
            searchResponse =  elasticsearchClient
                    .search(s -> s.query(supplier.get()), Map.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }


    public SearchResponse<Product> matchAllProductServices() {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplier();
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }

    public SearchResponse<Product> matchProductsWithName(String name) {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplierWithFields(name);
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }

    public SearchResponse<Product> matchProductsWithFuzzyName(String name) {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplierFuzzyName(name);
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }

    public SearchResponse<Product> matchProductsWithBoolQuery(Map<String, String> mapFields, Integer quantity) {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplierBoolQuery(mapFields, quantity);
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }

    public SearchResponse<Product> matchProductsWithMultiMatchQuery(String key, List<String> values) {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplierMultiMatchQuery(key, values);
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }

    public SearchResponse<Product> matchProductsBoolQueryFilter(Map<String, String> mapFields) {
        SearchResponse<Product> searchResponse = null;
        try {
            var supplier = ElasticSearchUtil.supplierBoolQueryFilter(mapFields);
            searchResponse =  elasticsearchClient
                    .search(s -> s
                            .index(PRODUCT_INDEX)
                            .query(supplier.get()), Product.class);

        } catch (Exception e) {
            log.error("Error while searching for all documents - {}", e.getMessage());
        }
        return searchResponse;
    }
}
